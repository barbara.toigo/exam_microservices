Barbara Toïgo

***Part 1: cloud deployment***

**Docker & containerization**

`podman build -t node-app .`
`podman run -p 8080:8080 -e APPLICATION_INSTANCE=example node-app`

Pour le mettre sur Docker Hub (ne fonctionne pas):

` podman container ls`
CONTAINER ID  IMAGE                           COMMAND               CREATED         STATUS         PORTS                   NAMES
10817bb1c701  localhost/node_app:latest       node src/count-se...  9 minutes ago   Up 9 minutes   0.0.0.0:8080->8080/tcp  sweet_hugle

` podman login -u barbaraisen`
Password: 
Login Succeeded!

` podman container commit 10817bb1c701 node:latest `
` podman image tag node:latest registry-host:5000/barbaraisen/node:latest `
` podman image push registry-host:5000/barbaraisen/node:latest`
Getting image source signatures
Error: trying to reuse blob sha256:7cea17427f83f6c4706c74f94fb6d7925b06ea9a0701234f1a9d43f6af11432a at destination: pinging container registry registry-host:5000: Get "https://registry-host:5000/v2/": dial tcp: lookup registry-host: no such host

J'ai essayé plusieurs méthodes pour Docker hub mais toujours cette erreur, je suis passée sur Google cloud:

`podman tag node_app gcr.io/hardy-symbol-408809/image_node`
`podman login -u _json_key -p "$(cat hardy-symbol-408809-0af474720973.json)" https://gcr.io`
Login Succeeded!

`podman tag node_app gcr.io/hardy-symbol-408809/image_node`
`podman push gcr.io/hardy-symbol-408809/image_node`
Getting image source signatures
Error: trying to reuse blob sha256:7cea17427f83f6c4706c74f94fb6d7925b06ea9a0701234f1a9d43f6af11432a at destination: Requesting bearer token: invalid status code from registry 400 (Bad Request)


J'ai ensuite `podman system prune --all` restart podman et réessayé mais toujours la même erreur, je ne sais pas quoi faire...

**Kubernetes**

` kubectl create deployment node-app --image=barbaraisen/docker-image_node`
` kubectl get deployments`
NAME              READY   UP-TO-DATE   AVAILABLE   AGE
node-app          0/1     1            0           25s

` kubectl get pods`
NAME                               READY   STATUS             RESTARTS        AGE
node-app-599d4d5bcb-t47f6          0/1     ImagePullBackOff   0               60s

`kubectl scale deployment node-app --replicas=3`
deployment.apps/node-app scaled

` kubectl apply -f deployment.yaml`
deployment.apps/node-app-deployment created

` kubectl expose deployment node-app --type=LoadBalancer --port=8080 --type=LoadBalancer`
service/node-app exposed
` minikube service node-app-deployment`
|-----------|---------------------|-------------|-----------------------------|
| NAMESPACE |        NAME         | TARGET PORT |             URL             |
|-----------|---------------------|-------------|-----------------------------|
| default   | node-app-deployment |        8080 | http://192.168.39.250:31406 |
|-----------|---------------------|-------------|-----------------------------|

❌  Fermeture en raison de SVC_UNREACHABLE : service not available: no running pod for service node-app-deployment found


***Part 2: Microservice architecture***

Q1: Une architecture microservice consiste à diviser une application en plusieurs services indépendants, alors qu'une application monolithique regroupe toutes les fonctionnalités en un seul gros bloc de code.

Q2:

Microservices :
- Plus facile à déployer ou à gérer en cas de problème, car un seul service est à gérer, pas l'application entière.
- Plus difficile à coder au début et plus compliqué de faire communiquer les différents services entre eux.

Monolithique :
- Plus simple à coder, car un seul bloc où tout communique déjà.
- En cas de problème, on doit revoir tout le code, et tous les services cessent de fonctionner le temps de gerer le problème.

Q3: Dans une architecture microservice, l'application doit être découpée en domaines métiers pour que chacun soit indépendant.

Q4: Le théorème CAP dit qu'un système distribué ne peut pas garantir simultanément la cohérence (Consistency), la disponibilité (Availability), et la tolérance aux partitions (Partitioning), et qu'on doit donc choisir parmi les trois.

Q5: La conséquence est qu'on doit choisir entre les trois selon l'application et les besoins du client.

Q6: 

Q7: "Statelessness" signifie que chaque requête d'un client au serveur est autonome et ne dépend pas des requêtes précédentes.

Q8: L'API Gateway (passerelle) sert à recevoir les requêtes du client et à les envoyer au microservice correspondant, elle est également utilisée pour l'authentification et la sécurité.

